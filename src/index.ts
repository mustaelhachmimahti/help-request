import './styles/index.css';
import axios, { AxiosRequestConfig } from 'axios';
import * as qs from 'qs';
//@ts-ignore
import { Chart, registerables } from 'chart.js';

const main = () => {
  const help = document.getElementById('help');
  const name = document.getElementById('select-name');
  const table = document.getElementById('table-body');
  const description = document.getElementById('ticket-name');
  const alert = document.getElementById('alert');
  const next = document.getElementById('next');
  const add = document.getElementById('add-user');
  const name_user = document.getElementById('name-user');
  const deleteAll = document.getElementById('delete');

  const getData = async (url: string): Promise<any> => {
    const data = await axios.get(url);
    return data;
  };

  const displayUser = async () => {
    const data = await getData(
      'https://web-help-request-api.herokuapp.com/users'
    );
    const user = data.data.data;
    user.forEach((e: any) => {
      name.innerHTML += `
            <option value="${e.id}">${e.username}</option>
            `;
    });
  };
  const alertBlank = () => {
    return (alert.innerHTML = `
  <div class="flex items-center bg-blue-500 text-white text-sm font-bold px-4 py-3" role="alert">
  <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
  <p>Vous ne pouvez pas laisser l'espace vide</p>
  </div>
  `);
  };

  const rowTable = (element: any, name: any) => {
    return `
    <tr>
        <td class="px-6 py-4 whitespace-nowrap">${element.id}</td>
        <td class="px-6 py-4 whitespace-nowrap">${name}</td>
        <td class="px-6 py-4 whitespace-nowrap">${element.subject}</td>
        <td class="px-6 py-4 whitespace-nowrap">${element.date}</td>
        <td class="px-6 py-4 whitespace-nowrap"><button value="${element.id}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Done</button></td>
    </tr>
`;
  };

  const displayTicket = async (): Promise<void> => {
    const data = await getData(
      'https://web-help-request-api.herokuapp.com/tickets'
    );
    const userData = await getData(
      'https://web-help-request-api.herokuapp.com/users'
    );
    const user = userData.data.data;
    const ticket = data.data.data;
    ticket.forEach((e: any) => {
      const name = user.map((el: any) => {
        if (e.users_id === el.id) {
          return el.username;
        }
      });

      table.innerHTML += rowTable(e, name);
    });

    //@ts-ignore
    const done = [...document.querySelectorAll('.bg-blue-500')];

    done.map((el: any) =>
      el.addEventListener('click', async function () {
        await axios.patch(
          `https://web-help-request-api.herokuapp.com/tickets/${el.value}`
        );
        location.reload();
      })
    );
  };

  const sendTicketData = async () => {
    if (
      (description as HTMLInputElement).value.length === 0 ||
      (name as HTMLInputElement).value.length === 0
    ) {
      alertBlank();
    } else {
      const headers: AxiosRequestConfig = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      };
      const res = await axios.post(
        'https://web-help-request-api.herokuapp.com/tickets',
        qs.stringify({
          subject: (description as HTMLInputElement).value,
          userId: (name as HTMLInputElement).value,
        }),
        headers
      );
      location.reload();
    }
  };

  help.addEventListener('click', sendTicketData);
  next.addEventListener('click', async function nextTicket() {
    const data = await getData(
      'https://web-help-request-api.herokuapp.com/tickets'
    );
    const ticket = data.data.data;
    await axios.patch(
      `https://web-help-request-api.herokuapp.com/tickets/${ticket[0].id}`
    );
    location.reload();
  });
  add.addEventListener('click', async function addNewUser() {
    if ((name_user as HTMLInputElement).value.length === 0) {
      alertBlank();
    } else {
      const headers: AxiosRequestConfig = {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      };
      const res = await axios.post(
        'https://web-help-request-api.herokuapp.com/users',
        qs.stringify({
          username: (name_user as HTMLInputElement).value,
          password: 'samePassword',
        }),
        headers
      );
      location.reload();
    }
  });
  displayTicket();
  displayUser();

  const diagram = async () => {
    const resUser = await getData(
      'https://web-help-request-api.herokuapp.com/users'
    );
    const resTickets = await getData(
      'https://web-help-request-api.herokuapp.com/tickets'
    );
    const dataTickets = resTickets.data.data;
    const dataUser = resUser.data.data;
    const array: any[] = [];
    dataTickets.forEach((el: any) => {
      dataUser.forEach((e: any) => {
        if (e.id === el.users_id) {
          array.push(e.username);
        }
      });
    });
    var diagramInfo = array.reduce(function (obj, b) {
      obj[b] = ++obj[b] || 1;
      return obj;
    }, {});
    const numberTickets = [];
    for (var key in diagramInfo) {
      numberTickets.push(diagramInfo[key]);
    }
    const arraytest: any = [];
    const test = Object.getOwnPropertyNames(diagramInfo).map(e => {
      arraytest.push(
        `rgb(${Math.floor(Math.random() * 255)},${Math.floor(
          Math.random() * 255
        )}, ${Math.floor(Math.random() * 255)})`
      );
    });
    const data = {
      labels: Object.getOwnPropertyNames(diagramInfo),
      datasets: [
        {
          label: 'My First Dataset',
          data: numberTickets,
          backgroundColor: arraytest,
          hoverOffset: 4,
        },
      ],
    };
    const config = {
      type: 'doughnut',
      data: data,
      options: {},
    };
    const myChart = new Chart(document.getElementById('myChart'), config);
  };

  diagram();

  deleteAll.addEventListener('click', async function deleteAll() {
    const data = await getData(
      'https://web-help-request-api.herokuapp.com/tickets'
    );
    const allTickets = data.data.data;
    allTickets.forEach(async (e: any) => {
      await axios.patch(
        `https://web-help-request-api.herokuapp.com/tickets/${e.id}`
      );
      location.reload();
    });
  });
};

main();
